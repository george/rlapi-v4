<?php
    namespace Ratelimited\Api\Router;

    class Response {
        public function __construct(string $response, $status_code = 200){
            header('content-type: application/json');
            if($status_code != 200)
            {
                http_response_code($status_code);
            }
            echo $response;
        }
    }
