<?php
    namespace Ratelimited\Api\Controllers;
    use Ratelimited\Api\Models\Users as UserModel;
    use Ratelimited\Api\Utils\SecurityUtils as Security;
    use Ratelimited\Api\Utils\AuthUtils as Auth;

    class Users {

        private $users;
        private $security;
        private $auth;

        public function __construct()
        {
            $this->users = new UserModel();
            $this->security = new Security();
            $this->auth = new Auth();
        }

        /**
         * Creates a user
         * 
         * @param string $username
         * @param string $email
         * @param string $password
         * @param mixed $opts
         * 
         * @return array from Model
         */
        public function create(string $username, string $email, string $password, $opts=null)
        {
            if(getenv('REGISTRATIONS_ENABLED')){
                // @TODO: Captcha verification if enabled.
                $creation = $this->users->create($username, $email, $password, $opts);

                return $creation;
            } else {
                return [
                    'succes' => false,
                    'error_msg' => 'Registrations are currently disabled.'
                ];
            }
        }

        /**
         * Verify a user's email
         */
        public function verify($verification_id)
        {
            if($this->security->isValidUUID($verification_id))
            {
                $verification = $this->users->verify($verification_id);

                return $verification;
            } else {
                http_response_code(400);
                return [
                    'success' => false,
                    'error_msg' => 'Invalid UUID Given.'
                ];
            }

        }

        /**
         * Gets all users that exist in the service
         */

        public function getAll()
        {
            if ( 1 == 1 ) // @todo: actual authorization here
            {
                return $this->users->getAll();
            } else {
                http_response_code(401);
                return [
                    'success' => false,
                    'error_msg' => 'Invalid auth.'
                ];
            }
        }

        public function getOne(String $userID)
        {
            if ( 1 == 1 ) // @todo: actual authorization here
            {
                return $this->users->getOne($userID);
            } else {
                http_response_code(401);
                return [
                    'success' => false,
                    'error_msg' => 'Invalid auth.'
                ];
            }
        }
    }
